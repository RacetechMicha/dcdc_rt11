/*
 * main.c
 *
 *  Created on: 07.03.2016
 *      Author: Michael Lipp
 */

/*#################################################################################################
	Fuer	: DCDC RT11
	Version	: 1.0
	autor	: Michael Lipp
	License	: GNU General Public License
	Hardware: AT90CAN128, 16MHz,
	Aufgabe :
//##################################################################################################
	kein watchdog moeglich ... sonst controller bei softwarereset im Bootloadermodus
//################################################################################################*/

#include "main.h"

volatile int16_t			timer_sendID			= 50,
							timer_update_Status		= 350;

union	Input_Status		Input;


void Initialisierung(void)
{
		wdt_disable();													//watchdog disable
		cli();															//disable interrupts

//####################################### Time in ms-- 8 Bit Timer0
	TCCR0A  	|= _BV(CS00)| _BV(CS01);								// 8bit -  Prescaller 64 - z�hler 250 Schritte --> Millisekunden
	TCNT0  		= 5;							 						// Timer 0, overflow nach 1 ms
	TIMSK0  	|= _BV(TOIE0);

//####################################### enable interrupts
	_delay_ms(100);
		sei();								// enable interrupts

// CAN-Empfang mit Interrupt-gest�tzem Einlesen der Nachrichten

	CAN_init(1000, RX);													// Bus mit Baudrate von 1000 kBaud initialisieren und Receive-Interrupts aktivieren


//Definition of Controller-Pins

	//Keine Pull-Up-Anschaltung notwendig, da alle Signale einen definierten Pegel haben m�ssten

		DDRC &= ~(1 << PC0);			//AIR Feedback
		DDRC &= ~(1 << PC1);			//Relay Feedback
		DDRC &= ~(1 << PC2);			//BSPD Current Trigger
		DDRC &= ~(1 << PC3);			//BSPD Pedal Trigger
		DDRC &= ~(1 << PC4);			//BMS SC Trigger
		DDRC &= ~(1 << PC5);			//IMD SC Trigger
}

ISR(SIG_OVERFLOW0)
{
	TCNT0  		= 	5;			 					// Timer 0, overflow nach 1 ms

	timer_update_Status--;	if (timer_update_Status		<=0)	{timer_update_Status	= 0;}
	timer_sendID--;			if (timer_sendID			<=0)	{timer_sendID			= 0;}
}


int main(void)
{
	Initialisierung();

	while(1)
	{
		if(timer_update_Status <= 0)
		{
			timer_update_Status = 10;
			Input_Update();
			send_CAN_Status();
		}

		if(timer_sendID <= 0)
		{
			timer_sendID = 500;
			send_CAN_ID();
		}

	}

	return 0;
}
