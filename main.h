/*
 * main.h
 *
 *  Created on: 08.03.2016
 *      Author: Michael Lipp
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "can.h"
#include "i2cmaster.h"
#include "uart.h"
#include "utils.h"
#include "globvars.h"


#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define uniq(LOW,HEIGHT)	((HEIGHT << 8)|LOW)			// 2x 8Bit 	--> 16Bit

#define sbi(ADDRESS,BIT) 	((ADDRESS) |= (1<<(BIT)))	// set Bit
#define cbi(ADDRESS,BIT) 	((ADDRESS) &= ~(1<<(BIT)))	// clear Bit
#define	toggle(ADDRESS,BIT)	((ADDRESS) ^= (1<<BIT))		// Bit umschalten

#define	bis(ADDRESS,BIT)	(ADDRESS & (1<<BIT))		// bit is set?
#define	bic(ADDRESS,BIT)	(!(ADDRESS & (1<<BIT)))		// bit is clear?

#define ROUND(x)    ((unsigned) ((x) + .5))				// runde eine positive Zahl

#ifndef	TRUE
	#define	TRUE	(1==1)
#elif !TRUE
	#error	fehlerhafte Definition fuer TRUE
#endif

#ifndef FALSE
	#define	FALSE	(1!=1)
#elif FALSE
	#error	fehlerhafte Definition fuer FALSE
#endif


#define AIR_is_high			(PINC & (1<<PINC0))
#define PreCharge_is_high	(PINC & (1<<PINC1))
#define BSPD_Curr_is_high	(PINC & (1<<PINC2))
#define BSPD_Ped_is_high	(PINC & (1<<PINC3))
#define BMS_is_high			(PINC & (1<<PINC4))
#define IMD_is_high			(PINC & (1<<PINC5))


//Funktionsprototypen CAN-Kommunikation

void send_CAN_ID (void);
void send_CAN_Status(void);
void Input_Update(void);
//void receive_CAN_interrupt(can_t msg);

enum CAN_Message_Objects
{
	MOb_Nr_DCDC_ID				= 0,
	MOb_Nr_DCDC_Status			= 1,
};

#endif /* MAIN_H_ */
