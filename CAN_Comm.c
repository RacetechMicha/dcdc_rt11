/*
 * Can_VCU.c
 *
 *  Created on: 27.07.2016
 *      Author: Michael
 */


#include "main.h"


void send_CAN_ID (void)
{
	can_t MOb_DCDC =
	{
			.idm	= 0xFFFFFFFF,
			.id		= DCDC_ID,
			.length	= 0
	};
	CAN_enableMOB(MOb_Nr_DCDC_ID, RECEIVE_DATA, MOb_DCDC);
	CAN_sendData(MOb_Nr_DCDC_ID, MOb_DCDC);
	CAN_disableMOB(MOb_Nr_DCDC_ID);
}

void send_CAN_Status(void)
{
	can_t MOb_DCDC_Status =
	{
			.idm	= 0xFFFFFFFF,
			.id		= DCDC_Status_ID,
			.length	= 1
	};

	MOb_DCDC_Status.data[0]			= Input.data[0];

	CAN_enableMOB(MOb_Nr_DCDC_Status, RECEIVE_DATA, MOb_DCDC_Status);
	CAN_sendData(MOb_Nr_DCDC_Status, MOb_DCDC_Status);
	CAN_disableMOB(MOb_Nr_DCDC_Status);
}

void Input_Update(void)
{
	Input.active.AIR		= AIR_is_high;
	Input.active.BMS		= BMS_is_high;
	Input.active.BSPD_Curr	= BSPD_Curr_is_high;
	Input.active.BSPD_Ped	= BSPD_Ped_is_high;
	Input.active.IMD		= IMD_is_high;
	Input.active.PreCharge	= PreCharge_is_high;
}

/*
SIGNAL(SIG_CAN_INTERRUPT1)
{
	uint8_t		save_canpage;
 	static		can_t message;


	// Aktuelle CANPAGE sichern
 	save_canpage	= CANPAGE;

    // Index des MOB ermitteln, der den Interrupt ausgel�st hat
	uint8_t mob 	= CAN_getMOBInterrupt();

	// Falls es kein g�ltiges MOB war abbrechen
	if(mob == NOMOB){
		return;
	}

	// Objekt das den Interrupt ausgel�st hat holen
	CAN_getMOB(mob);

	// Daten des MOBs aus CANMSG auslesen
	message			= CAN_getData();

	// Id der Nachricht holen
	message.id		= CAN_getID();

	// Inhalt der Nachricht in entsprechende, Code-interne Variablen schreiben
	receive_CAN_interrupt(message);

	// RXOK-Flag l�schen
	clearbit(CANSTMOB, RXOK);

	// MOB auf Empfang und CAN 2.0B Standard setzen
	CAN_setMode(RECEIVE_DATA);

    // CANPAGE wiederherstellen
	CANPAGE		= save_canpage;
}
*/
