/*
 * globvars.h
 *
 *  Created on: 22.05.2016
 *      Author: Michael
 */

#ifndef GLOBVARS_H_
#define GLOBVARS_H_

//Ausgehende Nachrichten DCDC

#define DCDC_ID					700			//CAN ID DCDC
#define DCDC_Status_ID			705			//CAN ID DCDC Status


union Input_Status
{
	struct byteorder_input_status
	{
		uint8_t AIR			:1;
		uint8_t PreCharge	:1;
		uint8_t BSPD_Curr	:1;
		uint8_t BSPD_Ped	:1;
		uint8_t BMS			:1;
		uint8_t IMD			:1;
	}active;
	uint8_t data[1];
};

extern union	Input_Status	Input;

#endif /* GLOBVARS_H_ */
